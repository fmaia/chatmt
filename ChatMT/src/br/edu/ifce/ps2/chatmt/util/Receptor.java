package br.edu.ifce.ps2.chatmt.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Receptor implements Runnable {
	private Socket canal;	
	public Socket getCanal() {
		return canal;
	}
	public void setCanal(Socket canal) {
		this.canal = canal;
	}
	@Override
	public void run() {
		try {
			ObjectInputStream entrada = 
				new ObjectInputStream(
						canal.getInputStream());
			while(true) {
				String msg = entrada.readUTF();
				JOptionPane.showMessageDialog(
						null, msg);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

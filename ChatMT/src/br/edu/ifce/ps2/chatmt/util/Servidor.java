package br.edu.ifce.ps2.chatmt.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
	public static void main(String[] args) {
		try {
			ServerSocket server = 
					new ServerSocket(12347);
			Socket cliente = server.accept();
			
			Transmissor t = new Transmissor();
			t.setCanal(cliente);
			Thread tt = new Thread(t);
			tt.start();
			
			Receptor r = new Receptor();
			r.setCanal(cliente);
			Thread tr = new Thread(r);
			tr.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}



